import 'bootstrap/dist/css/bootstrap.min.css';  //to use bootstrap 5
import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import Home from "./pages/Home.js";
import Products from './pages/Products';
import AppNavbar from './components/Navbar';
import ProductView from './components/ProductView';
import Login from './pages/Login';
import { useEffect, useState } from 'react';
import { UserProvider } from './UserContext';
import Logout from './pages/Logout';


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
  }, [user])



  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <>
        <Router>
          <div className="App">
            <AppNavbar />
            <Routes>
              <Route exact path="/" element={<Home />} />
              <Route exact path="/products" element={<Products />} />
              <Route exact path="/login" element={<Login />} />
              <Route exact path="logout" element={<Logout/>}   />
              <Route exact path="/productView/:productId" element={<ProductView />} />
              <Route exact path="/cart" />
            </Routes>
          </div>
        </Router>


      </>
    </UserProvider>
  );
}

export default App;
