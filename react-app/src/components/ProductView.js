import { useContext, useEffect, useState } from 'react'
import { useNavigate, useParams, Link } from 'react-router-dom';
import { Container, Card, Button, Row, Col, Form, InputGroup } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function ProductView() {

    const { user } = useContext(UserContext);
    // Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
    const navigate = useNavigate();

    //THe "useParams hook allows us to retrieve the courseId passed via URL"
    const { productId } = useParams();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [image, setImage] = useState("");
    const [price, setPrice] = useState("");
    const [quantity, setQuantity] = useState(0);

    function increment(){
        setQuantity(quantity + 1)
    }
    function decrement(){
        setQuantity(quantity - 1)
    }


    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then(res => res.json())
            .then(data => {
                setName(data.name);
                setDescription(data.description);
                setImage(data.image);
                setPrice(data.price);
            })
    }, [])


    const addToCart = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/placeOrder`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                products: {
                    productName: name,
                    quantity: quantity
                }
            })
        })
    }


    return (
        <Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Img variant="top" src={image} style={{ height: "40vh" }} />
                        <Card.Body >
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle >Price:  ₱{price}</Card.Subtitle>
                            <Card.Text></Card.Text>
                            <Card.Subtitle >Quantity:</Card.Subtitle>
                            <Card.Text>
                                <InputGroup style={{ width: '30%' }}>
                                    <Form.Control className='text-center'
                                        placeholder={quantity}
                                        aria-label="Recipient's username with two button addons"
                                    />
                                    <Button onClick={decrement} variant="outline-secondary">-</Button>
                                    <Button onClick={increment} variant="outline-secondary">+</Button>
                                </InputGroup>
                            </Card.Text>
                        </Card.Body>
                        <Card.Body className='text-center'>
                            {
                                (user.Id !== null) ?
                                    <Button onClick={() => addToCart(productId)} variant="primary" block>Add to Cart</Button>
                                    :
                                    <Link className="btn btn-danger btn-block" to="/login">Login</Link>
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}
