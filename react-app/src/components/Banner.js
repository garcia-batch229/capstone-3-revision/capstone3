import React from 'react';



export default function Banner() {
    return (
        <>
            <div className="container-fluid   ">
                <div className="row  height60 ">
                    <div className="col-12 mainImage1 ">
                        <div className="row" >
                            <div id="text" className='col-6 divCenter text-light'>
                                <div className='mt-5 pt-5'>
                                    <h1 className='mt-5'>Gaming Desktop</h1>
                                    <button type="button" class="btn btn-outline-light mt-5">Learn More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row height40 " >
                    <div className="col-md-4 mainImage2">
                        <div className="row">
                            <div className="col-12">
                                <div id="text" className='text-light divCenter' >
                                    <h1 className='mt-5'>GPU</h1>
                                    <button type="button" class="btn btn-outline-light mt-5">Learn More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 mainImage3">
                        <div className="row">
                            <div className="col-12">
                                <div id="text" className='text-light divCenter' >
                                    <h1 className='mt-5'>Coolers</h1>
                                    <button type="button" class="btn btn-outline-light mt-5">Learn More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 mainImage4">
                        <div className="row">
                            <div className="col-12">
                                <div id="text" className='text-light divCenter' >
                                    <h1 className='mt-5'>Graphic Cards</h1>
                                    <button type="button" class="btn btn-outline-light mt-5">Learn More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
