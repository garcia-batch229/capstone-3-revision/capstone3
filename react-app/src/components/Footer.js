import React from 'react'
import { LinkedinLogo, FacebookLogo, EnvelopeSimple, GitlabLogo } from "phosphor-react";

export default function Footer() {
    return (
        <div>
            <section id="contact" class=" bg-secondary text-white p-5">
                <div class="footer">
                    <div class="container ">
                        <div class="row justify-content-center">
                            <div class="col-md-6 text-center">
                                <h4 class="text-white">John Francis Garcia</h4>
                                <p>Web Developer</p>
                                <ul class="navbar-nav flex-row justify-content-center ">
                                    <li class="nav-item">
                                        <a class="text-light px-2" href="https://www.linkedin.com/in/john-francis-garcia-a743a0255/" target="_blank" rel="noreferrer"><LinkedinLogo size={32} /></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="text-light px-2" href="https://www.facebook.com/jfgarcia09" target="_blank" rel="noreferrer"><FacebookLogo size={32} /></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="text-light px-2" href="mailto:jfgarcia0911@gmail.com" target="_blank" rel="noreferrer"><EnvelopeSimple size={32} /></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="text-light px-2" href="https://gitlab.com/Xaero09" target="_blank" rel="noreferrer"><GitlabLogo size={32} /></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <h4 class="text-white text-center">Contact</h4>
                                <ul class="ul-last">
                                    <li class="text-center">Email: jfgarcia0911@gmail.com</li>
                                    <li class="text-center">Phone: 0929-131-8008</li>
                                    <li class="text-center">Address: Hinigaran, Negros Occidental</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-buttom">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-auto text-center">
                                <p class="mt-1 pt-1">John Francis Garcia</p>
                                <p>© All Rights Reserved 2023</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}
