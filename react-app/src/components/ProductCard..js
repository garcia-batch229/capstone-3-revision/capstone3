import React from "react";
import {Link} from 'react-router-dom'
export default function ProductCard({ productProps }) {

	const {_id, name, image, price} = productProps;

	return (

		<div className="col-11 col-md-6 col-lg-3 mx-0 mb-4" >
			<div className="card p-0 overflow-hidden h-100 shadow">
				<img src={image} alt="" className="card-img-top" style={{ width: "100%", height: "30vh", objectFit: 'contain' }} />
				<div className="card-body text-center">
					<h5 className="card-title" style={{ height: "10vh" }}>{name}</h5>
					<h5 className="card-text mt-4">Price: ₱ {price}</h5>
				</div>
				<div className="card-body text-center">
					<Link className="btn btn-primary" to={`/productView/${_id}`}>View Details</Link>
				</div>
			</div>
		</div>

	)
}
