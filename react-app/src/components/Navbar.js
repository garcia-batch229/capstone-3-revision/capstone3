import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { ShoppingCart } from "phosphor-react";
import UserContext from '../UserContext';


export default function AppNavbar() {

    const { user } = useContext(UserContext);

    return (
        <>
            <nav class=" navbar navbar-expand-lg navbar-lightfixed-top">
                <div class="container-fluid px-5">
                    <a class="navbar-brand" href="/" disabled >COMPSTRUCSTORE</a>
                    <button
                        class="navbar-toggler"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mx-auto  mb-lg-0">
                            <li class="nav-item ">
                                <NavLink className="nav-link " exact to="/">Home</NavLink>
                            </li>
                            <li class="nav-item ">
                                <NavLink className="nav-link " exact to="/products">Product</NavLink>
                            </li>

                            {
                                (user.id === null) ?
                                    <li class="nav-item ">
                                        <NavLink className="nav-link" exact to="/login">Login</NavLink>
                                    </li>
                                    :
                                    <li class="nav-item ">
                                        <NavLink className="nav-link" exact to="/logout">Logout</NavLink>
                                    </li>
                            }
                        </ul>
                        <div><NavLink className="nav-link" exact to="/cart"> <ShoppingCart size={32} /></NavLink></div>
                    </div>
                </div>
            </nav>




        </>
    );
}


