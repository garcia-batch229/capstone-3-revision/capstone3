import React from 'react'
import Banner from '../components/Banner.js'
import Footer from '../components/Footer.js'
export default function Home() {
    return (
        <>
         <Banner/>
         <Footer/>
        </>
    )
}
