import { useState, useContext } from 'react'
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import { Form, Button, InputGroup} from 'react-bootstrap';
import Swal from 'sweetalert2'


export default function Login() {

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    function loginUser(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                if (typeof data.access !== "undefined") {
                    localStorage.setItem('token', data.access);
                    retrieveUserDetails(data.access);

                    Swal.fire({
                        title: 'Login Succesfull',
                        icon: 'success',
                        text: 'Welcome to COMPSTRUCSTORE'
                    })

                    localStorage.setItem('email', email);

                    setUser({
                        email: localStorage.getItem('email')
                    })
                } else {
                    Swal.fire({
                        title: 'Authentication Failed',
                        icon: 'error',
                        text: 'Check your login details and try again!'
                    })


                }
                setEmail('');
                setPassword('');
            })

    }

    const retrieveUserDetails = token => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer $(token)`
            }
        })
            .then(res => res.json())
            .then(data => {

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
    }



    return (

        (user.id !== null) ?
            <Navigate to="/products" />
            :
            <Form onSubmit={(e) => loginUser(e)}>
                <section className="p-5 container shadow" style={{ width: "30vw", height: "60vh" }}>
                    <div className="row justify-content-center">

                        <Form.Group className="mb-3 " controlId="formBasicEmail">
                            <h2 className="my-5 text-center">Login</h2>
                            <Form.Label >Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required />

                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required />
                        </Form.Group>

                        <Form.Group className="mb-3 text-center"  controlId="formBasicPassword">
                        <Button xs={12}  className="my-5 mx-1"  style={{ width: '30%' }}controlId="submitBtn" variant="success " type="submit">Login</Button>
                        <Button xs={6}  className="my-5 mx-1" style={{ width: '30%' }}controlId="submitBtn" variant="success " type="submit">Sign Up</Button>
                        </Form.Group>




                    </div>
                </section>
            </Form>
    )
}
