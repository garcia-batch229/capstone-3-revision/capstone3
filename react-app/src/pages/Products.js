import { useEffect, useState } from 'react'
import ProductCard from '../components/ProductCard.';
export default function Products() {

  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products`)
      .then(res => res.json())
      .then(data => {
        setProducts(data.map(product => {          
          return (
            <ProductCard key={product._id}
              productProps={product} />

          )

        }))
      });


  }, [])


  return (
    <>
      <h1 className="text-center" mt-3>All Items</h1>
      <section className="py-4 container">
        <div className="row justify-content-center">
          {products}
        </div>
      </section>
    </>
  )
}
